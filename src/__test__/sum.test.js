const sum = require('./sum');
const fetchData = require('./fetchData');

//// ===> Using Matcher

test('adds 1 + 2 to equal 3', () => {
    expect(sum(1, 2)).toBe(3);
});

test('two plus two is four', () => {
    expect(2 + 2).toBe(4);
});

test('object assignment',() => {
    const data = {one : 1};
    data['two'] = 2;
    expect(data).toEqual({one: 1, two: 2});
})

test('adding positive number is not zero', () => {
    for (let a = 1; a < 10; a++) {
        for (let b = 1; b < 10; b++) {
            expect(a + b).not.toBe(0);
        }
    }
})

test('null', () => {
    const n = null;
    expect(n).toBeNull();
    expect(n).toBeDefined();
    expect(n).not.toBeUndefined();
    expect(n).not.toBeTruthy();
    expect(n).toBeFalsy();
  });
  
  test('zero', () => {
    const z = 0;
    expect(z).not.toBeNull();
    expect(z).toBeDefined();
    expect(z).not.toBeUndefined();
    expect(z).not.toBeTruthy();
    expect(z).toBeFalsy();
  });

  test('two plus two', () => {
      const value = 2 + 2;
      expect(value).toBeGreaterThan(3);
      expect(value).toBeGreaterThanOrEqual(3.5);
      expect(value).toBeGreaterThanOrEqual(4);
      expect(value).toBeLessThan(5);
      expect(value).toBeLessThanOrEqual(4.5);

      expect(value).toBe(4);
      expect(value).toEqual(4);
  });

  test('adding floating point number', () => {
      const value = 3.3 + 2.2;
      expect(value).toBeCloseTo(5.5);
  });

  test('there is no I in team', () => {
      const value = 'team';
      const query = 'I'
      expect(value).not.toMatch(query);
  })

  const shoppingList = [
    'diapers',
    'kleenex',
    'trash bags',
    'paper towels',
    'beer',
  ];

  test('test shopping list has beer on it', () => {
      expect(shoppingList).toContain('beer');
      expect(new Set(shoppingList)).toContain('beer');
  });

  // ===> Testing Asynchronous code

//   test('the data is peanut butter', () => {
//     return expect(fetchData()).resolves.toBe('peanut butter');
//   });

//   test('the fetch fails with an error', () => {
//     return expect(fetchData()).rejects.toMatch('error');
//   });

//// ===> Setup and Teardown


